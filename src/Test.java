import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Test {
    public static void main(String[] args) {
        //Стек
        Stack<String> stack = new Stack<>();

        //Заполним стек данными
        stack.push("PHP");
        stack.push("Java");
        stack.push("JavaScript");

        //Выведем стек через цикл
        for (Object lang : stack.toArray()) {
            System.out.println(lang);
        }
//        stack.forEach(System.out::println); //Java8+

        //Считаем последний элемент стека
        String peek = stack.peek();
        System.out.println("peek : " + peek);

        //Выведем стек обычным способом
        System.out.println(stack);

        //Извлечём последний элемент
        String pop = stack.pop();
        System.out.println("pop : " + pop);

        //Выведем стек обычным способом
        System.out.println(stack);

        //Очередь
        Queue<String> queue = new LinkedList<>();
        queue.add("Mint");
        queue.add("Ubuntu");
        queue.add("Debian");

        System.out.println();

//        queue.forEach(v -> System.out.println(v)); //Java8+
//        queue.forEach(System.out::println); //Java8+
        System.out.println(queue);

        peek = queue.peek();
        System.out.println("peek : " + peek);
        System.out.println(queue);

        String poll = queue.poll();
        System.out.println("poll : " + poll);
        System.out.println(queue);
    }
}
